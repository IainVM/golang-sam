package payload

import (
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
)

func init() {
	log.SetPrefix("response =>")
	log.SetFlags(log.Lmicroseconds | log.Lshortfile)
}

type Response struct {
	*events.APIGatewayProxyResponse
}

func NewResponse(payload interface{}, statusCode int) (*Response, error) {

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	response := &Response{
		APIGatewayProxyResponse: &events.APIGatewayProxyResponse{
			StatusCode: statusCode,
			Body:       string(jsonPayload),
		},
	}

	return response, nil
}

func NewInternalServerError(message string) *events.APIGatewayProxyResponse {
	return &events.APIGatewayProxyResponse{
		StatusCode: 500,
		Body:       message,
	}
}

func NewBadRequestError(message string) *events.APIGatewayProxyResponse {
	return &events.APIGatewayProxyResponse{
		StatusCode: 400,
		Body:       message,
	}
}

func NewNotFoundError(message string) *events.APIGatewayProxyResponse {
	return &events.APIGatewayProxyResponse{
		StatusCode: 404,
		Body:       message,
	}
}
