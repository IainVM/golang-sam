module gitlab.com/iainvm/golang-sam

require (
	github.com/aws/aws-lambda-go v1.23.0
	github.com/aws/aws-sdk-go v1.40.44
	github.com/docker/distribution v2.7.1+incompatible
	github.com/google/uuid v1.3.0 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	google.golang.org/genproto v0.0.0-20210917145530-b395a37504d4
)

go 1.16
