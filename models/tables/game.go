package tables

import (
	"log"

	"github.com/google/uuid"
	"gitlab.com/iainvm/golang-sam/models/mixins"
)

const gameTableName = "Games"

type Games []*Game

func (games Games) GetTableName() string {
	return gameTableName
}

type Game struct {
	*mixins.StandardColumns
	SolidsPlayer  *Player `json:"solidsPlayer"`
	StripesPlayer *Player `json:"stripesPlayer"`
	Winner        string  `json:"winner"`
}

type Player struct {
	UserId     *uuid.UUID `json:"userId"`
	WhiteFouls int        `json:"whiteFouls"`
	BlackFouls int        `json:"blackFouls"`
}

func NewGame() *Game {

	game := &Game{
		StandardColumns: mixins.NewStandardColumns(gameTableName),
	}

	log.Printf("Created game: {%+v}", game)

	return game
}
