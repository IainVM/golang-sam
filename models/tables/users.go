package tables

import (
	"log"

	"gitlab.com/iainvm/golang-sam/models/mixins"
)

const usersTableName = "Users"

type Users []*User

func (users Users) GetTableName() string {
	return usersTableName
}

type User struct {
	*mixins.StandardColumns
	Name string `json:"name"`
}

func NewUser() *User {

	user := &User{
		StandardColumns: mixins.NewStandardColumns(usersTableName),
	}

	log.Printf("Created user: {%+v}", user)

	return user

}
