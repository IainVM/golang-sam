package mixins

import (
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/iainvm/golang-sam/database/dynamodb"
)

type StandardColumns struct {
	tableName string     `json:"-"`
	Id        string     `json:"id"`
	CreatedOn *time.Time `json:"createdOn"`
	UpdatedOn *time.Time `json:"updatedOn"`
}

func (row *StandardColumns) Write() {
	dynamodb.CreateItem(row)
}

func (row *StandardColumns) GetTableName() string {
	return row.tableName
}

func NewStandardColumns(tableName string) *StandardColumns {

	now := time.Now().UTC()

	row := &StandardColumns{
		tableName: tableName,
		Id:        uuid.New().String(),
		CreatedOn: &now,
		UpdatedOn: &now,
	}

	log.Printf("Created row: %v", row)
	return row
}
