#!/bin/bash

aws dynamodb create-table --cli-input-json file://scripts/tables/games.json --endpoint-url http://localhost:8000
aws dynamodb create-table --cli-input-json file://scripts/tables/users.json --endpoint-url http://localhost:8000
