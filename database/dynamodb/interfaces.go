package dynamodb

type ReadWriteable interface {
	Writeable
	Readable
}

type Named interface {
	GetTableName() string
}

type Writeable interface {
	Named
	Write()
}

type Readable interface {
	Named
	// Read() *Readable
}
