package dynamodb

import (
	"log"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

var db *DB

type SessionCreationError struct{ error }

type DB struct {
	*dynamodb.DynamoDB
}

func init() {
	log.SetPrefix("dynamo =>")
	log.SetFlags(log.Lmicroseconds | log.Lshortfile)
}

func GetSession() (*DB, error) {

	if db == nil {

		isLocal := strings.ToLower(os.Getenv("AWS_SAM_LOCAL")) == "true"
		if isLocal {

			sess, err := session.NewSession(&aws.Config{
				Region:   aws.String("eu-west-1"),
				Endpoint: aws.String("http://dynamodb:8000"),
			})
			if err != nil {
				return nil, err
			}

			db = &DB{
				DynamoDB: dynamodb.New(sess),
			}

		} else {
			// Get AWS DynamoDB Session
			log.Println("NOT IMPLEMENTED D:")
		}
	}

	return db, nil
}

func ListTables() {
	result, err := db.ListTables(&dynamodb.ListTablesInput{})
	if err != nil {
		log.Println(err)
		return
	}

	log.Println("Tables:")
	for _, table := range result.TableNames {
		log.Println(*table)
	}
}

func CreateItem(item Writeable) {

	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		log.Fatalf("Got error marshalling new item: %s", err)
	}

	tableName := item.GetTableName()
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: &tableName,
	}

	log.Printf("%+v", av)
	_, err = db.PutItem(input)
	if err != nil {
		log.Fatalf("Got error calling PutItem: %+v", err)
	}
}

func GetAllFromTable(container Readable) {

	tableName := container.GetTableName()
	result, err := db.Scan(&dynamodb.ScanInput{
		TableName: &tableName,
	})

	if err != nil {
		log.Fatalf("Got error getting items from table: %s", err)
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, container)
	if err != nil {
		log.Fatalf("Got error unmarshalling items from db: %s", err)
	}
}

func GetById(id string, container Readable) {

	condition := map[string]*dynamodb.AttributeValue{
		"id": {
			S: aws.String(id),
		},
	}

	log.Printf("Getting by id: %+v", condition)

	GetItem(condition, container)
}

func GetItem(condition map[string]*dynamodb.AttributeValue, container Readable) {

	tableName := container.GetTableName()
	log.Printf("Getting Item from table: %+v", tableName)
	result, err := db.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key:       condition,
	})

	if err != nil {
		log.Fatalf("Got error getting items from table: %s", err)
	}

	err = dynamodbattribute.UnmarshalMap(result.Item, container)
	if err != nil {
		log.Fatalf("Got error unmarshalling items from db: %s", err)
	}
}
