package main

import (
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/iainvm/golang-sam/database/dynamodb"
	"gitlab.com/iainvm/golang-sam/models/tables"
	"gitlab.com/iainvm/golang-sam/payload"
)

func init() {
	log.SetPrefix("POST v1/users =>")
	log.SetFlags(log.Lmicroseconds | log.Lshortfile)
}

func post(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	// Parse request body
	user := tables.NewUser()

	if request.Body != "" {
		err := json.Unmarshal([]byte(request.Body), user)
		if err != nil {
			return *payload.NewBadRequestError("Error occured when parsing body"), nil
		}
	}

	// Connect to DB
	_, err := dynamodb.GetSession()
	if err != nil {
		return *payload.NewInternalServerError("DB Connection Failure"), nil
	}

	// Save data to DB
	user.Write()

	// Format Response
	response, err := payload.NewResponse(user, 200)
	if err != nil {
		return *payload.NewInternalServerError("Error Formatting Data"), nil
	}

	// Return Response
	return *response.APIGatewayProxyResponse, nil
}
