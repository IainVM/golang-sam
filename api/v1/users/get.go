package main

import (
	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/iainvm/golang-sam/database/dynamodb"
	"gitlab.com/iainvm/golang-sam/models/tables"
	"gitlab.com/iainvm/golang-sam/payload"
)

func get(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	// Connect to DB
	_, err := dynamodb.GetSession()
	if err != nil {
		return *payload.NewInternalServerError("DB Connection Failure"), nil
	}

	// Get data
	users := &tables.Users{}
	dynamodb.GetAllFromTable(users)

	// Create response
	response, err := payload.NewResponse(users, 200)
	if err != nil {
		return *payload.NewInternalServerError("Error Formatting Data"), nil
	}

	// Send response
	return *response.APIGatewayProxyResponse, nil

}
