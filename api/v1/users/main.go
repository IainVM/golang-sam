package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	switch request.HTTPMethod {
	case "GET":
		return get(request)
	case "POST":
		return post(request)
	}

	return events.APIGatewayProxyResponse{
		Body:       "",
		StatusCode: 405,
	}, nil
}

func main() {
	lambda.Start(handler)
}
