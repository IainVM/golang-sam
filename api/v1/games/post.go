package main

import (
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/iainvm/golang-sam/database/dynamodb"
	"gitlab.com/iainvm/golang-sam/models/tables"
	"gitlab.com/iainvm/golang-sam/payload"
)

func init() {
	log.SetPrefix("POST v1/games =>")
	log.SetFlags(log.Lmicroseconds | log.Lshortfile)
}

func post(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	// Parse request body
	game := tables.NewGame()

	if request.Body != "" {
		err := json.Unmarshal([]byte(request.Body), game)
		if err != nil {
			return *payload.NewBadRequestError("Error occured when parsing body"), nil
		}
	}

	// Connect to DB
	_, err := dynamodb.GetSession()
	if err != nil {
		return *payload.NewInternalServerError("DB Connection Failure"), nil
	}

	// Save data to DB
	game.Write()

	// Format Response
	response, err := payload.NewResponse(game, 200)
	if err != nil {
		return *payload.NewInternalServerError("Error Formatting Data"), nil
	}

	// Return Response
	return *response.APIGatewayProxyResponse, nil
}
