package main

import (
	"log"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/iainvm/golang-sam/database/dynamodb"
	"gitlab.com/iainvm/golang-sam/models/tables"
	"gitlab.com/iainvm/golang-sam/payload"
)

func get(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	pathParams := request.PathParameters
	game_id := pathParams["id"]

	log.Printf("Requesting game: %v", game_id)

	// Connect to DB
	_, err := dynamodb.GetSession()
	if err != nil {
		return *payload.NewInternalServerError("DB Connection Failure"), nil
	}

	// Get data
	game := tables.NewGame()
	dynamodb.GetById(game_id, game)

	// Create response
	response, err := payload.NewResponse(game, 200)
	if err != nil {
		return *payload.NewInternalServerError("Error Formatting Data"), nil
	}

	// Send response
	return *response.APIGatewayProxyResponse, nil
}
