.PHONY: build start

export AWS_PROFILE = playground
export AWS_REGION = eu-west-1

build:
	sam build

validate:
	sam validate

start: build
	sam local start-api --docker-network pool_default

full-restart:
	docker-compose down -v
	docker-compose up -d
	sleep 1
	make local_tables
	make start

test:
	cd hello-world; go test

local_tables:
	./scripts/create_local_tables.sh

req_games:
	http POST localhost:3000/v1/games
	http GET localhost:3000/v1/games

req_users:
	http GET localhost:3000/v1/users
